<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>updateProfileInvalidToken</name>
   <tag></tag>
   <elementGuidId>74b9a819-994f-4843-92fc-c69dabaf6d11</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>XAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiN2I2N2NhOTY3YTY1ZGNiNTRhZThhYmQ5MjQ5NDEwMDU3OTYzMzliNjA2MTRjNTUwZjE0Nzk5MTMwNDYxMTExNzNhMzU0ZmVkNzVhNWQzNDEiLCJpYXQiOjE2ODg5NTQyMDIuNTY4MDgsIm5iZiI6MTY4ODk1NDIwMi41NjgwODMsImV4cCI6MTcyMDU3NjYwMi41NTkwNTYsInN1YiI6IjYwMCIsInNjb3BlcyI6W119.jNoKvhlD7cb_bhoJtKffvEvX2CgtEHsGS0fTBgjVgXqEEwEsGrugRvvW2nWX0iw24XpoGDhFK05qAA8B4kt4URMaR0GuwKxUQ2UbUJUd-e-kSybit8PT5nCv6NMg5q00dbJA9JC981z0BMspwUs6MBXGJaId2EhM9hpdauQUhFSHenm545JFFU-wfjImsL77h3ETsUOAk8u9khPxpczkJ1TBRea8ETsV-PIKCrgtNH1XIChJjKZEebvjbgos6XbJjzYgdytklGbe-BOTv5EFuVHl2yw3EGBPbomS9nlUvZzqjRDSDzOW0x0ki_SCenjpotCiwlI9gXUL_zXzGl7cDKob6wAx3ykbb3jBUmPbkdg_2mKOnfkV8txG5mAUDA3vlZOsrwE3wh_lFsEpmJr5Ral9wGyS07LQ6S_A8DTCrrzqjn-Y_oxJUvKdERNiYHyddAWB5bJsP7akKlmTk2_anyUrNuoqk-SjhGWvkJDwC-I7CsaS3h6Z5D6FOyUS2iyhclMZNdBaFv282nuB2qwDtdJCDhumRPM1LU0XkH3XHvhXjaI-qQgCFP2-OXd6-gkjoE-tmpkKXVEZIkDQlIZ6mhaaUq40eeeCOgpYutEc0_w2P9TGi7NuRmY_DrKxa7-YypEJsJ2zGR35SVxJ9dKS4bBJklz4KYaOmGGRXNieo2g</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;name&quot;,
      &quot;value&quot;: &quot;Finn&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;whatsapp&quot;,
      &quot;value&quot;: &quot;081234567890&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;birth_date&quot;,
      &quot;value&quot;: &quot;2000-02-29&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;photo&quot;,
      &quot;value&quot;: &quot;C:\\Users\\Arsan Mukti\\Downloads\\Adventure Time 1.jpg&quot;,
      &quot;type&quot;: &quot;File&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;bio&quot;,
      &quot;value&quot;: &quot;This is my bio&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;position&quot;,
      &quot;value&quot;: &quot;Learner&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
      <webElementGuid>8ffdbca8-8499-4aed-84c6-7004b634838f</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer XAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiN2I2N2NhOTY3YTY1ZGNiNTRhZThhYmQ5MjQ5NDEwMDU3OTYzMzliNjA2MTRjNTUwZjE0Nzk5MTMwNDYxMTExNzNhMzU0ZmVkNzVhNWQzNDEiLCJpYXQiOjE2ODg5NTQyMDIuNTY4MDgsIm5iZiI6MTY4ODk1NDIwMi41NjgwODMsImV4cCI6MTcyMDU3NjYwMi41NTkwNTYsInN1YiI6IjYwMCIsInNjb3BlcyI6W119.jNoKvhlD7cb_bhoJtKffvEvX2CgtEHsGS0fTBgjVgXqEEwEsGrugRvvW2nWX0iw24XpoGDhFK05qAA8B4kt4URMaR0GuwKxUQ2UbUJUd-e-kSybit8PT5nCv6NMg5q00dbJA9JC981z0BMspwUs6MBXGJaId2EhM9hpdauQUhFSHenm545JFFU-wfjImsL77h3ETsUOAk8u9khPxpczkJ1TBRea8ETsV-PIKCrgtNH1XIChJjKZEebvjbgos6XbJjzYgdytklGbe-BOTv5EFuVHl2yw3EGBPbomS9nlUvZzqjRDSDzOW0x0ki_SCenjpotCiwlI9gXUL_zXzGl7cDKob6wAx3ykbb3jBUmPbkdg_2mKOnfkV8txG5mAUDA3vlZOsrwE3wh_lFsEpmJr5Ral9wGyS07LQ6S_A8DTCrrzqjn-Y_oxJUvKdERNiYHyddAWB5bJsP7akKlmTk2_anyUrNuoqk-SjhGWvkJDwC-I7CsaS3h6Z5D6FOyUS2iyhclMZNdBaFv282nuB2qwDtdJCDhumRPM1LU0XkH3XHvhXjaI-qQgCFP2-OXd6-gkjoE-tmpkKXVEZIkDQlIZ6mhaaUq40eeeCOgpYutEc0_w2P9TGi7NuRmY_DrKxa7-YypEJsJ2zGR35SVxJ9dKS4bBJklz4KYaOmGGRXNieo2g</value>
      <webElementGuid>c623c1e4-e283-4538-b10d-071f5a7bc011</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.5</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.baseUrl}/updateprofile</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 302)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
