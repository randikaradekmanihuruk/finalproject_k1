<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>updateProfileInvalidPhoto</name>
   <tag></tag>
   <elementGuidId>49bdabbc-5301-4d5b-88f3-8eea38bec6ca</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiYzk3Mzg2YTEwNjI2YzQ5NGViN2JlYzA2MGQwZGRlM2I4NjRlZGQwYjY2ZDZjZjIxNmZkMDdhOTlkYjQzYWU1ZjkwODczZWZjYjAyOGM3MzgiLCJpYXQiOjE2ODg3MTk5NDguOTY2MDQ4LCJuYmYiOjE2ODg3MTk5NDguOTY2MDUxLCJleHAiOjE3MjAzNDIzNDguOTYzMjc5LCJzdWIiOiI2MDAiLCJzY29wZXMiOltdfQ.SfbzL8dyMMRSVq1jEJvhxN8ZltmfYY3o2kt_rftm_aQbhrTEdk9hLRs7jMUWepveXv2y3GC1gvxEikWohbJ-zOSQYgklpqnm2V3A2iDGip2-eMm95zVxdk8mxLqA9Y84qeMt8oFXVqmP7Dwlswx8cew4jMfa_RbVpyhZ7j9j0mUGPiC2oiUvfdZzJ7cFHbd8oIZScwyMYFTRsiVuDRxZ2EmATLCCP7KjE9bCMafqGs29WHZhm-dbIsJszaqkrVtF3Xq3AhI9gz8VXSB0SFi0w9cJPTBTL0NcwdpwH-I1VrFGxOBV2YuB_IQcX-F_t7fSpiw2rRIKnFoq58SIzJL8GX_Guk_WZV4Siuox90FWT4_V-jj9NT8MyCRhh6-iE9SrJyGX1GEgB5cMH1aHmYXCffDerhmUA6E8eziLFDBTtKITonsZJy_zUP6hE4a79vptNryyvA0AyJrRhdHHtZXx9PMRmRPlhriBi3wT5ov_Hd95f8wbKfqj7STeT-BHm0PON3kktyIRAo-SLZM218HR43skcp3Ke3Uxk-Uz6ybUSvDRRsPJ28GpGFdtuw0zHuTfGK2uR92yHEr1YH7QK_cjmgp0edpyDRZ6CNoYxWulAyxCgWQ6otlzk0ur3hvncL2biMv7aHlXFpY2n4ojbEP4o9qFBX3mwMu3qeS4RfS87KI</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>false</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;name&quot;,
      &quot;value&quot;: &quot;Randika Radek&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;whatsapp&quot;,
      &quot;value&quot;: &quot;08123456789&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;birth_date&quot;,
      &quot;value&quot;: &quot;2000-05-22&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;photo&quot;,
      &quot;value&quot;: &quot;D:\\55418892-RandikaRadekManihuruk-4IA09.pdf&quot;,
      &quot;type&quot;: &quot;File&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;bio&quot;,
      &quot;value&quot;: &quot;FreshGraduate&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;position&quot;,
      &quot;value&quot;: &quot;Tester&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/json</value>
      <webElementGuid>15ee5a78-4776-417a-939a-57a8f5ea0ebf</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${GlobalVariable.token}</value>
      <webElementGuid>d5618281-a7a9-4282-9450-dbf96379fa98</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.5</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${GlobalVariable.baseUrl}/updateprofile</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()

WS.verifyResponseStatusCode(response, 200)
WS.verifyElementPropertyValue(response, 'data', &quot;success&quot;)</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
