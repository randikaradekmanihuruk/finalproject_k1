<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>titleTransaksi Sukses</name>
   <tag></tag>
   <elementGuidId>c6aca542-9bac-4df6-9b13-5cf8b29b91b5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-success</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='wrap']/div[2]/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9c7ff2e7-9603-4115-be2a-99ed649a265b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-success</value>
      <webElementGuid>9752423c-977f-4083-90b3-b9241289bb2c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Transaksi Sukses</value>
      <webElementGuid>8b566c18-bd01-4439-b54a-e9bb39eaff2f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;wrap&quot;)/div[@class=&quot;container&quot;]/div[@class=&quot;alert alert-success&quot;]</value>
      <webElementGuid>9e7dbfcb-9143-49ce-9ab0-41fb789c41ae</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='wrap']/div[2]/div[2]</value>
      <webElementGuid>680acce0-15d7-4b6e-982c-89d9a1e02193</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BCA KlikPay'])[1]/following::div[1]</value>
      <webElementGuid>573ab89a-8130-4acd-b4eb-633a36533a0f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Indomaret Endpoint'])[1]/following::div[3]</value>
      <webElementGuid>317d28d6-6429-4638-9cbf-9fce256e30c5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kembali ke Website Merchant'])[1]/preceding::div[1]</value>
      <webElementGuid>5beb3113-d8dc-4548-b151-f2b1444f0520</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::div[5]</value>
      <webElementGuid>5779d12b-3862-48b7-870a-e72e59de4244</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Transaksi Sukses']/parent::*</value>
      <webElementGuid>0fa2b887-9f04-4ac9-8547-e0a4e0a9ccfd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/div[2]</value>
      <webElementGuid>03ec38f8-93bf-4085-bf6f-370fe9555528</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Transaksi Sukses' or . = 'Transaksi Sukses')]</value>
      <webElementGuid>e83a245c-8d64-4f1b-81e4-59f9a4468eaf</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
