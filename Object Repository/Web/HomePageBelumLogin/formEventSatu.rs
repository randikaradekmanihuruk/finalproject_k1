<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>formEventSatu</name>
   <tag></tag>
   <elementGuidId>5209d167-55a8-4bf3-943a-09c62a4ad703</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-body-homepage']/div[6]/ul/li/div/div[2]/h6[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h6</value>
      <webElementGuid>fe890139-680c-46df-8d8c-bf07522a7198</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>new_headline_6</value>
      <webElementGuid>a6aeadb1-abba-44d5-803d-df3309c62af8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                Day 3: Predict using Machine Learning</value>
      <webElementGuid>234ff880-18b9-4b18-b041-838526af40c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-body-homepage&quot;)/div[@class=&quot;online-course-block&quot;]/ul[@class=&quot;list-online-course-container&quot;]/li[@class=&quot;list-online-course-item&quot;]/div[@class=&quot;cardBootcamp list-online-course-item-container&quot;]/div[@class=&quot;containerCardBootcamp&quot;]/h6[@class=&quot;new_headline_6&quot;]</value>
      <webElementGuid>b6c702d7-89b4-4bbc-8828-416ab68bbce9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-body-homepage']/div[6]/ul/li/div/div[2]/h6[2]</value>
      <webElementGuid>b6267b66-0818-49e6-94b0-39e0e5dd07e3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EVENT'])[1]/following::h6[1]</value>
      <webElementGuid>d4a01fac-7a49-43df-ad39-932ce767304c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Scientist Market Leader Company in Automotive Industry'])[1]/following::h6[2]</value>
      <webElementGuid>5aeb23f3-30eb-4deb-bcc5-58e0f544352f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Ziyad Syauqi Fawwazi'])[2]/preceding::h6[1]</value>
      <webElementGuid>d867c4f8-54d6-4aa2-a38e-310d224a7f53</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Data Scientist Market Leader Company in Automotive Industry'])[2]/preceding::h6[1]</value>
      <webElementGuid>ecf7c1b7-f84f-4f99-916c-1e506d531f88</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Day 3: Predict using Machine Learning']/parent::*</value>
      <webElementGuid>6d253175-59d7-4f40-9f97-fbbee00da0ed</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div[2]/h6[2]</value>
      <webElementGuid>b9713acd-6a96-4154-87ce-a9ffcf43dd1e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h6[(text() = '
                                Day 3: Predict using Machine Learning' or . = '
                                Day 3: Predict using Machine Learning')]</value>
      <webElementGuid>77ebe264-0c9f-49b6-8ee6-5aa5ec893afb</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
