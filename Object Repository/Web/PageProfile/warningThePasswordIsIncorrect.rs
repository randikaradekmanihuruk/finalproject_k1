<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>warningThePasswordIsIncorrect</name>
   <tag></tag>
   <elementGuidId>f55c933b-9ae2-4ac1-bbbb-45d7f5e34458</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>small</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div[2]/form/div/span/small</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>small</value>
      <webElementGuid>392e2330-bd79-46a1-8a91-e46f8457bca7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>The password is incorrect.</value>
      <webElementGuid>7333852f-9501-42e7-b24a-2f57f02925f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/form[1]/div[@class=&quot;form-group&quot;]/span[@class=&quot;invalid-feedback&quot;]/small[1]</value>
      <webElementGuid>02bc5eb3-ede3-40cf-b83d-accc68184bd2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div[2]/form/div/span/small</value>
      <webElementGuid>868ff5ef-131e-4ba8-8249-2695e292b7b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Old Password'])[1]/following::small[1]</value>
      <webElementGuid>0faca087-15f4-448b-a7a7-644d604631bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Change Password'])[1]/following::small[1]</value>
      <webElementGuid>0c2420d5-c795-494a-b22a-0609e57a10bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Password'])[1]/preceding::small[1]</value>
      <webElementGuid>3303e8cf-d6c8-4ca8-842a-b64793839ac3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The password must be at least 8 characters.'])[1]/preceding::small[1]</value>
      <webElementGuid>6036a0cb-e871-4460-8552-54484c2889aa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='The password is incorrect.']/parent::*</value>
      <webElementGuid>aa300e22-3478-462b-a636-4f15a26a1826</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//small</value>
      <webElementGuid>41fc35f3-d5f7-42ab-a962-97355214e331</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//small[(text() = 'The password is incorrect.' or . = 'The password is incorrect.')]</value>
      <webElementGuid>569218c5-0847-4bf2-855d-23dc577154a5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
