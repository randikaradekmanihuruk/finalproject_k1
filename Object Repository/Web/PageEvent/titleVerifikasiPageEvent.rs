<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>titleVerifikasiPageEvent</name>
   <tag></tag>
   <elementGuidId>16238db9-3dae-4eff-bca1-47b8f44ed0db</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#jumbotronTitle</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h1[@id='jumbotronTitle']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h1</value>
      <webElementGuid>53468823-0f1e-43f0-8983-9476c5372a2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>jumbotronTitle</value>
      <webElementGuid>a2d58c15-d495-4905-88a5-fe2cd8f011b4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>CODING.ID Event
            </value>
      <webElementGuid>7d6ac31d-57dc-48b1-8dda-61e775979099</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;jumbotronTitle&quot;)</value>
      <webElementGuid>2b179d93-251c-456b-937f-49addbfc1840</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//h1[@id='jumbotronTitle']</value>
      <webElementGuid>7e851bb2-7498-44e6-8156-33f0e26978c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Events'])[4]/preceding::h1[1]</value>
      <webElementGuid>dd8f699e-5818-4dc1-bb6c-7a45b3491529</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Day 3: Predict using Machine Learning'])[1]/preceding::h1[1]</value>
      <webElementGuid>44a1706f-f2e6-444f-badd-d4f3c3760808</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='CODING.ID Event']/parent::*</value>
      <webElementGuid>dc836284-71cb-45db-ac84-ed4991900636</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h1</value>
      <webElementGuid>a3d69426-9060-4d85-ab1b-abb259e7eec2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h1[@id = 'jumbotronTitle' and (text() = 'CODING.ID Event
            ' or . = 'CODING.ID Event
            ')]</value>
      <webElementGuid>4317d416-69e0-499d-9d5a-d73e30f327d3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
