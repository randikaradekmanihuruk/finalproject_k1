<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>buttonPopupVerifikasi</name>
   <tag></tag>
   <elementGuidId>792fcaa1-7136-45ae-83a3-d746c9dd7b32</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>form > button.btn.btn-block.btn-primary</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Modal_Success']/div/div/div/div/form/button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>5001f1ad-f4c2-40a4-b4f2-b5772386b868</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-block btn-primary</value>
      <webElementGuid>2187977e-2685-4bd5-883c-4d7ede3aab12</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Verifikasi</value>
      <webElementGuid>2fabf1fa-4399-4768-9adf-e47e169ba38f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Modal_Success&quot;)/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;wm-modallogin-form wm-login-popup&quot;]/form[1]/button[@class=&quot;btn btn-block btn-primary&quot;]</value>
      <webElementGuid>fccd9588-f25b-4aec-8ed3-68eb55b8e5d5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Modal_Success']/div/div/div/div/form/button</value>
      <webElementGuid>6fd63550-a3ec-4a28-b0a6-1414954abb79</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email harus terverifikasi'])[1]/following::button[1]</value>
      <webElementGuid>d34ff472-d114-4eec-bd15-53d2965fe3fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='×'])[1]/following::button[1]</value>
      <webElementGuid>3ea92865-9973-4f40-9875-1100ad5e1865</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='×'])[2]/preceding::button[1]</value>
      <webElementGuid>e795aab8-eebf-4922-9ba4-3296a80b0367</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About CODING.ID'])[1]/preceding::button[2]</value>
      <webElementGuid>ee2c8a6d-08b1-4ffe-be73-3dfa92bf8bfe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Verifikasi']/parent::*</value>
      <webElementGuid>dbc2d8d3-7e1d-4a87-8263-ce2333f5d284</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/form/button</value>
      <webElementGuid>fc15e4da-d6fa-4d52-bd92-8d7cbaa5df08</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = 'Verifikasi' or . = 'Verifikasi')]</value>
      <webElementGuid>9341926c-5051-427e-9e20-7784e5a5f4ef</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
