<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>warningLinkVerifikasiBaruTelahDikirim</name>
   <tag></tag>
   <elementGuidId>d6134ffe-6af6-4bb2-a97e-be76a509fdc8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.alert.alert-success</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Verifikasi Email'])[1]/following::div[6]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>475f6491-6fd9-4dbc-a250-4746a07c56b3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alert alert-success</value>
      <webElementGuid>e721eb81-606f-4fe8-aa3f-a2a9c10fa61b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>alert</value>
      <webElementGuid>07f2b909-444a-47cc-b885-b128bcd7e2e9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                            Link verifikasi baru telah dikirim ke alamat email Anda. (mohon periksa juga kotak spam)
                                        </value>
      <webElementGuid>8efcb265-438c-43ca-857d-296e2e0187ea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[@class=&quot;no-htmlimports no-flash no-proximity no-applicationcache blobconstructor blob-constructor cookies cors customprotocolhandler dataview eventlistener geolocation history no-ie8compat json notification queryselector serviceworker customevent postmessage svg templatestrings typedarrays websockets no-xdomainrequest webaudio webworkers no-contextmenu cssall audio canvas canvastext contenteditable emoji olreversed no-userdata video no-vml webanimations webgl adownload audioloop canvasblending todataurljpeg todataurlpng todataurlwebp canvaswinding no-ambientlight hashchange inputsearchevent pointerevents no-hiddenscroll mathml unicoderange no-touchevents no-unicode no-batteryapi no-battery-api crypto no-dart gamepads fullscreen indexeddb indexeddb-deletedatabase intl pagevisibility performance pointerlock quotamanagement requestanimationframe raf vibrate no-webintents no-lowbattery getrandomvalues backgroundblendmode cssanimations backdropfilter backgroundcliptext appearance exiforientation audiopreload&quot;]/body[1]/div[@class=&quot;wm-main-wrapper&quot;]/div[@class=&quot;wm-main-section&quot;]/div[@class=&quot;container&quot;]/div[1]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;alert alert-success&quot;]</value>
      <webElementGuid>e048d5c8-3a89-43fa-be91-9f71f60da3a7</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Verifikasi Email'])[1]/following::div[6]</value>
      <webElementGuid>ee244793-d193-4d7d-bf8f-07035b86b32f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Verifikasi alamat email Anda'])[1]/preceding::div[1]</value>
      <webElementGuid>63d43777-3d57-435f-896c-99515669b852</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kirim Ulang'])[1]/preceding::div[2]</value>
      <webElementGuid>ae556fe3-517f-4626-b235-9db181693d5c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Link verifikasi baru telah dikirim ke alamat email Anda. (mohon periksa juga kotak spam)']/parent::*</value>
      <webElementGuid>1a2bd006-fb5c-4779-85b2-6ddb5e0876d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div[2]/div/div/div/div/div</value>
      <webElementGuid>6b397a92-ae62-4806-99fc-d218b1de1242</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                                            Link verifikasi baru telah dikirim ke alamat email Anda. (mohon periksa juga kotak spam)
                                        ' or . = '
                                            Link verifikasi baru telah dikirim ke alamat email Anda. (mohon periksa juga kotak spam)
                                        ')]</value>
      <webElementGuid>5543287d-406f-4239-93c9-ac528a394910</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
