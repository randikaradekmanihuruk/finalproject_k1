<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textPopupEmailHarusTerverifikasi</name>
   <tag></tag>
   <elementGuidId>a2c6a9a0-6a08-4aa7-8276-4eb758277ca8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.wm-color</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Modal_Success']/div/div/div/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>44c1e0e5-bed0-4302-bc5c-e1bbbd89454c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wm-color</value>
      <webElementGuid>b70c4842-0f4f-4d46-b8f6-ada40aa0715f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                            Email harus terverifikasi
                                                    </value>
      <webElementGuid>4deda1fd-c4a7-4e24-8bca-478dedbca927</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Modal_Success&quot;)/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/div[@class=&quot;wm-modallogin-form wm-login-popup&quot;]/span[@class=&quot;wm-color&quot;]</value>
      <webElementGuid>e706fb59-ecea-46d4-b74f-0c1c239b9721</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Modal_Success']/div/div/div/div/span</value>
      <webElementGuid>ab676df1-a2f5-4b89-83df-9ffe741d7097</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='×'])[1]/following::span[1]</value>
      <webElementGuid>a45b573f-0fb0-4ff5-9596-fbedb727df5d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Buat akun'])[1]/following::span[1]</value>
      <webElementGuid>58561b77-5bc2-487a-91ea-97ddfa787819</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Verifikasi'])[1]/preceding::span[1]</value>
      <webElementGuid>fb1e5b55-657d-4cf2-9948-bd49c121db04</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='×'])[2]/preceding::span[1]</value>
      <webElementGuid>c65d243c-a541-4365-b0e2-b39ba659bdfc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Email harus terverifikasi']/parent::*</value>
      <webElementGuid>eaee7789-26b6-4c32-b15d-659cb692fc9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/span</value>
      <webElementGuid>0730abca-eec1-4192-b463-fc844c58cb22</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
                                                            Email harus terverifikasi
                                                    ' or . = '
                                                            Email harus terverifikasi
                                                    ')]</value>
      <webElementGuid>d1412b7c-c11b-44e5-8a81-db4b9616e4c6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
