<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>hoverMenuProfileIcon</name>
   <tag></tag>
   <elementGuidId>4e395cae-5a35-4aa2-ba24-cd37a6fa77c9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>ul.wm-dropdown-menu</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbar-collapse-1']/ul/li[7]/ul</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>ul</value>
      <webElementGuid>4fb527c0-e880-4456-aa94-c3828cfcaf62</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>wm-dropdown-menu</value>
      <webElementGuid>9d60e1ce-7356-495d-bcde-07b875cb2ff3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                                                                                    My
                                                                    Course
                                                            
                                                            Checkout
                                                            
                                                            My Account
                                                                
                                                            
                                                                                                                Logout
                                                        
                                                    </value>
      <webElementGuid>fc624763-e0a4-41af-bc07-253440598f84</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-collapse-1&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;___class_+?26___&quot;]/ul[@class=&quot;wm-dropdown-menu&quot;]</value>
      <webElementGuid>b934bec1-6522-48b2-9950-b9fe418437b2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbar-collapse-1']/ul/li[7]/ul</value>
      <webElementGuid>8c82f941-f98f-4677-bcef-5e56ed9051fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kontak'])[1]/following::ul[1]</value>
      <webElementGuid>487336fd-1b7a-45dc-b536-406e5b3d2d1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Blog'])[1]/following::ul[1]</value>
      <webElementGuid>bbf623b5-f2d8-4189-80d9-e2012c18ba9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[7]/ul</value>
      <webElementGuid>ef814f3d-7feb-4ba5-8bcf-9cfc006d4484</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//ul[(text() = '
                                                                                                                    My
                                                                    Course
                                                            
                                                            Checkout
                                                            
                                                            My Account
                                                                
                                                            
                                                                                                                Logout
                                                        
                                                    ' or . = '
                                                                                                                    My
                                                                    Course
                                                            
                                                            Checkout
                                                            
                                                            My Account
                                                                
                                                            
                                                                                                                Logout
                                                        
                                                    ')]</value>
      <webElementGuid>ff765a64-7448-4386-8e29-36b178fd3a24</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
