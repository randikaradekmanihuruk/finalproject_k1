<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>frameBayar</name>
   <tag></tag>
   <elementGuidId>8c7bb5fe-c22e-411b-9dc6-5cfb3776e075</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#snap-midtrans</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//iframe[@id='snap-midtrans']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
      <webElementGuid>a22b339a-329b-4220-b801-81b2e6380efc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://app.sandbox.midtrans.com/snap/v1/pay?origin_host=https://demo-app.online&amp;digest=61c8e0bd5e408bcd3a5bdf52c35c32eca5210736155959d33cf4c53c7e5d99d7&amp;client_key=SB-Mid-client-aQuCzf_xly4hqcSY#/</value>
      <webElementGuid>913ab9fa-b514-43f8-afe6-1803e12b1d0b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>snap-midtrans</value>
      <webElementGuid>5938aff9-6548-488d-bba1-fd3f8bd5c32d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>popup_1688045978307</value>
      <webElementGuid>992065b6-9483-4cd3-ac25-c15d68f87535</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;snap-midtrans&quot;)</value>
      <webElementGuid>522b8769-dc8a-426f-a6a3-3fffac72095d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//iframe[@id='snap-midtrans']</value>
      <webElementGuid>b9e13e46-d22f-4e79-8910-4759f838d5c7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//iframe</value>
      <webElementGuid>33b40b3b-b537-46f7-b9f7-36e187383686</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//iframe[@src = 'https://app.sandbox.midtrans.com/snap/v1/pay?origin_host=https://demo-app.online&amp;digest=61c8e0bd5e408bcd3a5bdf52c35c32eca5210736155959d33cf4c53c7e5d99d7&amp;client_key=SB-Mid-client-aQuCzf_xly4hqcSY#/' and @id = 'snap-midtrans' and @name = 'popup_1688045978307']</value>
      <webElementGuid>8fb9806d-aa28-463a-ab68-c71db71fa909</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
