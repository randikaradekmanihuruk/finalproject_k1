<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>buttonBCAKlikPay</name>
   <tag></tag>
   <elementGuidId>bc45088f-8bbb-4d58-ac23-fda260ec6bea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div:nth-of-type(5) > a.list</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='application']/div/div[5]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>e612d673-09ab-4aa4-9f4b-fe31e480f803</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>list</value>
      <webElementGuid>6a04c477-a92c-4d42-9844-f931ab415c60</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>list-item</value>
      <webElementGuid>205d1cef-9529-49fc-b9c6-a621329823c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#/bca-klikpay</value>
      <webElementGuid>ad5da325-a194-4727-b1f4-cd6d1e52dced</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>BCA KlikPay</value>
      <webElementGuid>b4a98b16-b097-4ded-aabe-5845de298444</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;application&quot;)/div[@class=&quot;page-container scroll&quot;]/div[5]/a[@class=&quot;list&quot;]</value>
      <webElementGuid>94091457-99b3-4453-8119-9b1837fc09c0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ref_element</name>
      <type>Main</type>
      <value>Object Repository/Web/FrameBayar/frameBayar</value>
      <webElementGuid>ddab8683-d34c-4bf5-ab62-1fecf0bb67a3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='application']/div/div[5]/a</value>
      <webElementGuid>9a85adf0-e698-4479-ae64-6213a39e1ef2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ShopeePay'])[1]/following::a[1]</value>
      <webElementGuid>3a7f2477-d864-4216-b070-f4d8b9e536e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bank transfer'])[1]/following::a[2]</value>
      <webElementGuid>7f3a811d-1ba8-4ab5-873d-408ddedfe75a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='OCTO Clicks'])[1]/preceding::a[1]</value>
      <webElementGuid>a7f3d3aa-0f4c-4909-8c89-6893242ae295</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '#/bca-klikpay')]</value>
      <webElementGuid>eccf63cb-38f6-4a52-8171-f64f729104a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/a</value>
      <webElementGuid>14d4eecb-77d8-4264-ab94-03b9550e7bfb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#/bca-klikpay' and (text() = 'BCA KlikPay' or . = 'BCA KlikPay')]</value>
      <webElementGuid>70f1b282-a40a-409d-b3c3-0d17692c548b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
