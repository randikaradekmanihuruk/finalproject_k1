<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>buttonKirimUlang</name>
   <tag></tag>
   <elementGuidId>cd98a91b-0238-4b23-8b31-c05c2e194f5e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#btnCounter</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='btnCounter']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>795442b3-f594-4df4-a5b0-947a6eca7c1c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>59706e46-691c-48ab-875c-91ca2c83cc2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>btnCounter</value>
      <webElementGuid>968e467e-ba4a-48b5-a43e-80221f3c5afa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary p-0 m-0 align-baseline</value>
      <webElementGuid>55001614-d099-4164-b875-b87324551a41</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Kirim Ulang</value>
      <webElementGuid>8458c86e-9175-491b-bba7-5253ff1fdd2c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;btnCounter&quot;)</value>
      <webElementGuid>58bb8db7-814f-4492-b836-6c9d786d123c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='btnCounter']</value>
      <webElementGuid>398d3faf-5200-4b0e-bb97-695c344e4327</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Verifikasi alamat email Anda'])[1]/following::button[1]</value>
      <webElementGuid>d48cb3f9-b57f-4f4d-af89-b5ef611949d3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Verifikasi Email'])[1]/following::button[1]</value>
      <webElementGuid>2c3e9bba-facb-4f46-a361-6ea2ebda7356</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About CODING.ID'])[1]/preceding::button[1]</value>
      <webElementGuid>ea1cb5a2-2e8f-408b-8abf-a8d95b177755</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Site Map'])[1]/preceding::button[1]</value>
      <webElementGuid>ff46c699-c824-4c74-bb97-9ad07a9cd842</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Kirim Ulang']/parent::*</value>
      <webElementGuid>20d59a83-0de2-4b3d-afdd-2241620b790a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/form/button</value>
      <webElementGuid>22b06966-283f-433b-82bd-5113e9ebdd42</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'submit' and @id = 'btnCounter' and (text() = 'Kirim Ulang' or . = 'Kirim Ulang')]</value>
      <webElementGuid>1a7ee53f-f398-408e-adec-2d6d5001dd35</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
