<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>buttonDaftar</name>
   <tag></tag>
   <elementGuidId>cc9852ba-58fe-4a18-8406-9238db83543e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='buttonRegisterTrack']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#buttonRegisterTrack</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>1954dbd9-1603-48cc-b6c1-591dc80b7c81</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonRegisterTrack</value>
      <webElementGuid>d45b8b39-5b66-49dc-8ae0-e24424797535</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>submit</value>
      <webElementGuid>110e2284-f6bc-4a45-b53e-74e3034308a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-block btn-primary</value>
      <webElementGuid>a15626f6-befa-41c3-8a60-35bd6b04c7ae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                            Daftar
                                        </value>
      <webElementGuid>a56ae271-67d4-4360-8baf-f204f5813777</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonRegisterTrack&quot;)</value>
      <webElementGuid>0f7b4e34-9859-41de-b663-f27df129a556</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='buttonRegisterTrack']</value>
      <webElementGuid>558bf87b-c960-48ba-8759-46ca2309e5d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='syarat dan ketentuan'])[1]/following::button[1]</value>
      <webElementGuid>33f4f427-57df-46ce-bee8-4bf07d92a255</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Konfirmasi kata sandi'])[1]/following::button[1]</value>
      <webElementGuid>a8770acf-0563-4038-b27c-28c4a3181e9e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Daftar']/parent::*</value>
      <webElementGuid>e2c92ab6-80bf-4eb6-ac4a-ba865d975ae7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div/button</value>
      <webElementGuid>aaae5ae4-f42a-45f7-8a6a-fa68f272bd95</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'buttonRegisterTrack' and @type = 'submit' and (text() = '
                                            Daftar
                                        ' or . = '
                                            Daftar
                                        ')]</value>
      <webElementGuid>f6b08604-f3cf-4395-822e-e6b3bef00da7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
