<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>buttonSilahkanMasuk</name>
   <tag></tag>
   <elementGuidId>611e302d-352c-4448-9151-b6dfa4bd06ea</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='buttonGoInTrack']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#buttonGoInTrack</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>7a1df1bc-9c8c-4f5a-b806-40df980afd4f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>buttonGoInTrack</value>
      <webElementGuid>5e4d09f4-ee77-4aa6-a17d-658dc5c97f6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/login</value>
      <webElementGuid>87c52276-8fec-42c8-ae19-b71ef2158fcf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Silahkan
                                        Masuk</value>
      <webElementGuid>dbef7ed2-4064-41b4-b008-4831bb3c87df</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;buttonGoInTrack&quot;)</value>
      <webElementGuid>547f4abe-777f-4134-89e1-a391e5b1ad1f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='buttonGoInTrack']</value>
      <webElementGuid>118743dc-48f8-48f4-9d40-ba22f8fefd91</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Silahkan
                                        Masuk')]</value>
      <webElementGuid>93d3eab1-6d40-4ffa-9f75-6ed8b393d023</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='About CODING.ID'])[1]/preceding::a[1]</value>
      <webElementGuid>dadd101c-c33e-4609-bfec-c7b2907174d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Site Map'])[1]/preceding::a[1]</value>
      <webElementGuid>ecb00b64-1e0b-49ce-bd8e-6aa8b87be97e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>(//a[contains(@href, '/login')])[5]</value>
      <webElementGuid>594c068c-4520-4640-9532-aff7051e0f17</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[10]/a</value>
      <webElementGuid>4ebe3d27-a4a4-4994-ba45-92e213f06f7d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@id = 'buttonGoInTrack' and @href = '/login' and (text() = 'Silahkan
                                        Masuk' or . = 'Silahkan
                                        Masuk')]</value>
      <webElementGuid>b77019c5-d623-4908-b845-e7935a902046</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
