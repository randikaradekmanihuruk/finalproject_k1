import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.github.javafaker.Faker as Faker
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Faker faker = new Faker(new Locale('in-ID'))

String email = faker.internet().emailAddress()

WebUI.callTestCase(findTestCase('Web/Register/WEB-000 OpenPageBuatAkun'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Web/PageBuatAkun/inputNama'), 'Komang Komaladi123')

WebUI.setText(findTestObject('Web/PageBuatAkun/inputTanggalLahir'), '12-Oct-2015')

WebUI.setText(findTestObject('Web/PageBuatAkun/inputEmail'), email)

WebUI.setText(findTestObject('Web/PageBuatAkun/inputWhatsapp'), '081234567890')

WebUI.setText(findTestObject('Web/PageBuatAkun/inputKataSandi'), 'Password')

WebUI.setText(findTestObject('Web/PageBuatAkun/inputKonfirmasiKataSandi'), 'Password')

WebUI.check(findTestObject('Web/PageBuatAkun/checkboxSetuju'))

WebUI.click(findTestObject('Web/PageBuatAkun/buttonDaftar'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('Web/PageBuatAkun/buttonDaftar'), 1)

WebUI.delay(1)

