import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

TestData testDataWebLogin = findTestData('Data Files/DataWebLogin')

for (int rowData = 1; rowData <= testDataWebLogin.getRowNumbers(); rowData++) {	
	String email = testDataWebLogin.getValue(1, rowData);
	String password = testDataWebLogin.getValue(2, rowData)
	
	WebUI.click(findTestObject('Web/HomePageBelumLogin/buttonMasuk'), FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.setText(findTestObject('Web/PageLogin/inputEmail'), email, FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.setText(findTestObject('Web/PageLogin/inputKataSandi'), password, FailureHandling.CONTINUE_ON_FAILURE)
	
	WebUI.click(findTestObject('Web/PageLogin/buttonLogin'), FailureHandling.CONTINUE_ON_FAILURE)
	
	if (email == 'ahmat1@mailinator.com') {
		WebUI.verifyElementVisible(findTestObject('Web/PageLogin/textPopupEmailHarusTerverifikasi'), FailureHandling.CONTINUE_ON_FAILURE)
	} else {
		WebUI.verifyElementVisible(findTestObject('Web/PageLogin/iconProfile'), FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.click(findTestObject('Web/PageLogin/iconProfile'), FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.click(findTestObject('Web/PageLogin/buttonLogout'), FailureHandling.CONTINUE_ON_FAILURE)
		WebUI.verifyElementVisible(findTestObject('Web/HomePageBelumLogin/buttonBuatAkun'), FailureHandling.CONTINUE_ON_FAILURE)
	}
	
}