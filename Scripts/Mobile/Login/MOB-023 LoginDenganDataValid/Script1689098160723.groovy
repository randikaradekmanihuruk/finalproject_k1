import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import internal.GlobalVariable as GlobalVariable

Mobile.tap(findTestObject('Mobile/HomePageBelumLogin/buttonLoginHere'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Mobile/LoginPage/inputEmail'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.sendKeys(findTestObject('Mobile/LoginPage/inputEmail'), 'penggunabaru00@yahoo.com', FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Mobile/LoginPage/inputPassword'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.sendKeys(findTestObject('Mobile/LoginPage/inputPassword'), 'inipenggunabaru', FailureHandling.CONTINUE_ON_FAILURE)

Mobile.hideKeyboard(FailureHandling.CONTINUE_ON_FAILURE)

Mobile.tap(findTestObject('Mobile/LoginPage/buttonLogin'), 0, FailureHandling.CONTINUE_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Mobile/HomePageLogin/iconChart'), 0, FailureHandling.STOP_ON_FAILURE)

