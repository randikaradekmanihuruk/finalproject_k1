import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Mobile/Register/MOB-001 BukaPageRegister'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.scrollToText('Lets join our community!', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Mobile/RegisterPage/inputNama'), 0)

Mobile.sendKeys(findTestObject('Mobile/RegisterPage/inputNama'), 'ahmat')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Mobile/RegisterPage/inputEmail'), 0)

Mobile.sendKeys(findTestObject('Mobile/RegisterPage/inputEmail'), 'ahmat@mailinator.com')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Mobile/RegisterPage/inputWhatsapp'), 0)

Mobile.sendKeys(findTestObject('Mobile/RegisterPage/inputWhatsapp'), '081234567894')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Mobile/RegisterPage/inputKataSandi'), 0)

Mobile.sendKeys(findTestObject('Mobile/RegisterPage/inputKataSandi'), 'sandisandi1')

Mobile.hideKeyboard()

Mobile.tap(findTestObject('Mobile/RegisterPage/inputKonfirmasiKataSandi'), 0)

Mobile.sendKeys(findTestObject('Mobile/RegisterPage/inputKonfirmasiKataSandi'), 'sandisandi1')

Mobile.hideKeyboard()

Mobile.checkElement(findTestObject('Mobile/RegisterPage/CheckBoxItem'), 0)

Mobile.scrollToText('Daftar', FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Mobile/RegisterPage/buttonDaftar'), 0)

Mobile.verifyElementExist(findTestObject('Mobile/RegisterPage/buttonDaftar'), 0)

